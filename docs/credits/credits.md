---
author: Votre nom
title: 👏 Crédits
---

Le site est hébergé par  [la forge des communs numériques éducatifs](https://docs.forge.apps.education.fr/){:target="_blank" }.

![AEIF](./assets/images/logo_aeif_300.png){width=7%}    
Le modèle du site a été créé par l'  [Association des enseignantes et enseignants d'informatique de France](https://aeif.fr/index.php/category/non-classe/){target="_blank"}.  


Le site est construit avec [`mkdocs`](https://www.mkdocs.org/){target="_blank"} et en particulier [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/){target="_blank"}, et surtout [Pyodide-Mkdocs-Theme](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/){target="_blank"} pour la partie Python nécessaire pour les QCM.

😀 Un grand merci à Frédéric Zinelli, et Vincent-Xavier Jumel qui ont réalisé la partie technique de ce site.   
Merci également à Charles Poulmaire pour ses relectures attentives et ses conseils judicieux.

